echo 'stdin=serial
stdout=serial
stderr=serial
ethact=FEC0
loadaddr=0x70800000
loadaddr_ramdisk=0x70C00000
bootdev=2
rawtable=0xF40000
bootcmd=mmc read 2 ${loadaddr} 0x800 0x1400;mmc read 2 ${loadaddr_ramdisk} 0x2800 0x1F4;bootm ${loadaddr} ${loadaddr_ramdisk}
bootargs=console=ttymxc2,115200 init=/init bootdev=2 rawtable=0xF40000' |
mkenvimage -p 0x00 -s 131072 -o normal_env -

echo 'stdin=serial
stdout=serial
stderr=serial
ethact=FEC0
loadaddr=0x70800000
loadaddr_ramdisk=0x70C00000
bootdev=2
rawtable=0xF40000
bootcmd=mmc read 2 ${loadaddr} 0x3000 0x1400;bootm ${loadaddr}
bootargs=root=/dev/mmcblk2p1 rootfstype=ext4 ro rootwait init=/linuxrc console=ttymxc4,115200 bootdev=2 rawtable=0xF40000 bootmode=recovery' |
mkenvimage -p 0x00 -s 131072 -o recovery_env -

echo 'stdin=serial
stdout=serial
stderr=serial
ethact=FEC0
loadaddr=0x70800000
loadaddr_ramdisk=0x70C00000
bootdev=0
rawtable=0xF40000
bootcmd=mmc read 0 ${loadaddr} 0x2 0x74D8;bootm ${loadaddr}
bootargs=root=/dev/mmcblk0p2 rootfstype=ext4 rw rootwait init=/linuxrc console=ttymxc4,115200 bootdev=0 rawtable=0xF40000' |
mkenvimage -p 0x00 -s 131072 -o herman_env -

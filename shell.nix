{ pkgs ? import <nixpkgs> {} }:

(pkgs.buildFHSUserEnv {
  name = "buildroot";
  targetPkgs = pkgs: with pkgs; [
    # from buildroot doc
    which
    gnumake
    binutils
    diffutils
    gcc13 # locked
    bash
    patch
    gzip
    bzip2
    perl
    cpio
    unzip
    rsync
    file
    bc
    findutils
    wget

    # experimentally found
    autoconf
    automake
    ncurses.dev
    libxcrypt
  ];
}).env

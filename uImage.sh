mkimage -d initramfs.gz -n 'Normal Rootfs' -A ARM -T RAMDisk -C none -a 70308000 -e 70308000 uImageRamdisk
# Normal Kernel and Recovery Kernel have an additional optional section.
mkimage -d zImageRecovery -n 'Recovery Kernel' -A ARM -C none -a 70308000 -e 70308000 uImageRecovery
